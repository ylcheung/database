# database versioning

This repository used to document the versioning of certain database schema, which managed by Flyway (by redgate).

To use this one, several things needed to be done:

- flyway.conf
- sql/ directory

When the SQL schema is ready to deploy, the repository should be verged into `deploy` branch.

With help of Gitlab Runner, the migration will run automatically to deploy the migration script.

At this moment(20200808), this repository is fixed to do migration, undo migration is limited to Flyway Pro(which is priced).

## flyway.conf

This file instructs the Flyway which database should be connected to perform operation

common used config are described in [flyway.conf](/flyway.conf)

Full list of Migration config listed here:

[https://flywaydb.org/documentation/commandline/migrate](https://flywaydb.org/documentation/commandline/migrate)

## sql/ directory

SQL versioning script stored in this directory, which versioned by filename.

And the naming rules of migration SQL files listed here:

[https://flywaydb.org/documentation/migrations#naming](https://flywaydb.org/documentation/migrations#naming)

## SQL files

Ideally, this repo should support most types of SQL databases.

But database driver should be indicated in [flyway.conf](/flyway.conf).
