BEGIN;

-- here demos some operations 

-- create tables -----------------------------------------------
CREATE TABLE "public"."testing" ( 
	"id" Character Varying( 2044 ) NOT NULL,
	PRIMARY KEY ( "id" ),
	CONSTRAINT "unique_transactions_id" UNIQUE( "id" ) );
 ;
-- -------------------------------------------------------------

-- create index" -----------------------------------------------
CREATE INDEX "index" ON "public"."testing" USING btree( "id" Asc NULLS Last );
-- -------------------------------------------------------------

COMMIT;